//
//  DetailedViewController.swift
//  Todo
//
//  Created by Alfred Ayitey Ayi-Bonte on 4/10/16.
//  Copyright © 2016 Alfred Ayi-Bonte. All rights reserved.
//

import UIKit

class DetailedViewController: UIViewController {
    @IBOutlet weak var display: UITextView!
    let task : Task = Task()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = task.name
        self.navigationController?.navigationBar.topItem?.title = "Back"
        display.text = task.note

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
