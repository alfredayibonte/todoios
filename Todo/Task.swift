//
//  Task.swift
//  Todo
//
//  Created by Alfred Ayitey Ayi-Bonte on 4/9/16.
//  Copyright © 2016 Alfred Ayi-Bonte. All rights reserved.
//

import Foundation
import RealmSwift

class Task : Object {
    dynamic var name = ""
    dynamic var note = ""
    dynamic var createdAt = NSDate()
    dynamic var isCompleted = false
    
    
    // Specify properties to ignore (Realm won't persist these)
    
    //  override static func ignoredProperties() -> [String] {
    //    return []
    //  }

    
    
}