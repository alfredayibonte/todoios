//
//  databaseHelper.swift
//  Todo
//
//  Created by Alfred Ayitey Ayi-Bonte on 4/9/16.
//  Copyright © 2016 Alfred Ayi-Bonte. All rights reserved.
//

import Foundation
import RealmSwift

class DatabaseHelper{
    
    func saveToDatabase(task: Task)  {
        //do some shit here
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            let uiRealm = try! Realm()
            try! uiRealm.write({ () -> Void in
                uiRealm.add(task)
            })
            
        }
    }
    
    
    func getAllTask() -> Results<Task> {
        let uiRealm = try! Realm()
        let tasks = uiRealm.objects(Task)
        return tasks
      
    }
    


}
