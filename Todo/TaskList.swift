//
//  TaskList.swift
//  Todo
//
//  Created by Alfred Ayitey Ayi-Bonte on 4/10/16.
//  Copyright © 2016 Alfred Ayi-Bonte. All rights reserved.
//

import Foundation
import RealmSwift
class TaskList: Object {
    
    dynamic var name = ""
    dynamic var createdAt = NSDate()
    let tasks = List<Task>()
    
    // Specify properties to ignore (Realm won't persist these)
    
    //  override static func ignoredProperties() -> [String] {
    //    return []
    //  }
}
