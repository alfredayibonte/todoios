//
//  MainViewController.swift
//  Todo
//
//  Created by Alfred Ayitey Ayi-Bonte on 4/7/16.
//  Copyright © 2016 Alfred Ayi-Bonte. All rights reserved.
//

import UIKit
import RealmSwift

class MainViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    var isAddTaskViewVisible = false
    var task = Task()
    var db = DatabaseHelper()
    var lists : Results<Task>!
    @IBOutlet weak var uiTableView: UITableView!

    @IBOutlet weak var descriptionView: UITextView!
    @IBOutlet weak var titleText: UITextField!
    @IBAction func addTaskButton(sender: UIButton) {
        resignResponders()
        getTextAndSaveToDB()
    }
    @IBOutlet weak var todoTableView: UITableView!
    
    @IBOutlet weak var addTodoView: UIView!
    @IBAction func todoSegmentControl(sender: UISegmentedControl){
       
        switch sender.selectedSegmentIndex {
        case 0:
           showTableView()
        case 1:
            self.navigationItem.title = "Form"
            isAddTaskViewVisible = true
            todoTableView.hidden = true
            addTodoView.hidden = false
            self.titleText.becomeFirstResponder()

        default:
            break;
        }
        
    
    }
    
    func showTableView (){
        self.navigationItem.title = "Todos"
        todoTableView.hidden = false
        addTodoView.hidden = true
        resignResponders()
    
    }
    //table view stuff here
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
 
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("taskCell")! as UITableViewCell
        let task = lists[indexPath.row]
        cell.textLabel?.text = task.name
        let format = NSDateFormatter()
        format.dateFormat = "EEE dd-MM-yyyy  hh:mm:ss a"
        let date = format.stringFromDate(task.createdAt)
        cell.detailTextLabel?.text = "\(date)"
        return cell
    }
    //end of table view stuff
    func reloadTable(){
        showTableView()
        lists = db.getAllTask()
        uiTableView.setEditing(false, animated: true)
        uiTableView.reloadData()
        
     
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        resignResponders()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func resignResponders(){
        titleText.resignFirstResponder()
        descriptionView.resignFirstResponder()
    }
    func getTextAndSaveToDB(){
        let title : String = titleText.text!
        let description = descriptionView.text!
        if(isValidFields(title, description: description)){
            saveToDB(title, description: description)
            reloadTable()
            emptyTextFields()
        }else{
            showAlert()
            
        }
    }
    func emptyTextFields() {
        titleText.text = ""
        descriptionView.text = ""
    }
    
    func showAlert(){
        
        let alertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
        let alertCtl = UIAlertController(title: "Empty field", message: "One or More fields needs to be filled", preferredStyle: .Alert)
        alertCtl.addAction(alertAction)
        self.presentViewController(alertCtl, animated: true, completion: nil)
    }
    
    func isValidFields(title : String, description : String) -> Bool {
        return (!title.isEmpty) && (!description.isEmpty)
    }
    
    func saveToDB(title:String, description:String) {
        let task = Task()
        task.name = title
        task.note = description
        db.saveToDatabase(task)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.uiTableView.delegate = self
        self.uiTableView.dataSource = self
        reloadTable()
    }
    
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using 
        let controller = segue.destinationViewController as! DetailedViewController
        let indexPath = self.uiTableView.indexPathForSelectedRow! as NSIndexPath
        let task = lists[indexPath.row]
        controller.task.name = task.name
        controller.task.note = task.note
        // Pass the selected object to the new view controller.
    }
    

}
